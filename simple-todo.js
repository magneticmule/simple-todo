// simple-todos.js

Tasks = new Mongo.Collection('tasks');

if (Meteor.isClient) {
  // This code only runs on the client
  Template.body.helpers({
    tasks: function () {
      return Tasks.find({}, {sort: {createdAt: -1}});
    }
  });


  Template.body.events({
    "submit .new-task": function (event) {
    // This function is called when the new task form is submitted

    var text = event.target.text.value;
    var image = event.target.image.value;
    var sound = event.target.sound.value;

    Tasks.insert({
      text: text,
      image: image,
      sound: sound,
      createdAt: new Date(),            // current time
      owner: Meteor.userId(),           // _id of the logged in user
      username: Meteor.user().username  // the name of the logged in user
    });

    // Clear form
    event.target.text.value = "";
    event.target.image.value = "";
    event.target.sound.value = "";

    // Prevent default form submit
    return false;
  }
});

  Template.task.events({
    "click .delete": function (event) {
      Tasks.remove(this._id);
      console.log("Task with ID: " + this._id + " has been deleted");
    },

    "click .add-image": function (event) {
      var image = Template.body.event.target.image.value;
      if (( image !==null)  || ( image!=="" )) {
        Tasks.update(this._id, {$set: {image: ""}});
        console.log(this._id);
      } else {
        console.log("Empty!");
      }
    }
  });


}